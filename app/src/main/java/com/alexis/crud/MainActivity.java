package com.alexis.crud;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.alexis.crud.adapter.ListaAdapter;
import com.alexis.crud.domain.Test;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class MainActivity extends AppCompatActivity {

    private EditText etUno, etDos;
    private Button btnAgregar;
    private RecyclerView rvLista;
    private FirebaseDatabase database;
    private DatabaseReference testRef;
    private List<Test> lstTest = new ArrayList<>();
    private ListaAdapter listaAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        etUno = (EditText) findViewById(R.id.etTextoUno);
        etDos = (EditText) findViewById(R.id.etTextoDos);
        btnAgregar = (Button) findViewById(R.id.btnAgregar);
        rvLista = (RecyclerView) findViewById(R.id.rvlist);

        database = FirebaseDatabase.getInstance();
        testRef = database.getReference("test");

        LinearLayoutManager linearLayout = new LinearLayoutManager(this);
        rvLista.setLayoutManager(linearLayout);
        rvLista.setHasFixedSize(true);


    }

    @Override
    protected void onResume() {
        super.onResume();

        btnAgregar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (!validarCampos()) return;
                Test test = new Test();
                String id = testRef.push().getKey();
                test.setId(id);
                test.setValor1(etUno.getText().toString());
                test.setValor2(etDos.getText().toString());
                testRef.child(id).setValue(test);
                Toast.makeText(getApplicationContext(),"Agregado",Toast.LENGTH_LONG).show();
                limpiar();
            }
        });

        testRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                lstTest.clear();
                for(DataSnapshot snapshot : dataSnapshot.getChildren()){
                    Test test = snapshot.getValue(Test.class);
                    lstTest.add(test);
                }
                listaAdapter = new ListaAdapter(getApplicationContext(), MainActivity.this, lstTest);
                rvLista.setAdapter(listaAdapter);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }


    private void limpiar() {
        etUno.setText(null);
        etDos.setText(null);
        etUno.requestFocus();
    }

    private boolean validarCampos() {
        boolean valor = true;
        if (etUno.getText() == null) {
            etUno.setError("Ingrese campo");
            valor = false;
        }
        if (etDos.getText() == null) {
            etDos.setError("Ingrese campo");
            valor = false;
        }
        return valor;
    }

    public void eliminar(Test test) {
        testRef.child(test.getId()).removeValue();
    }

    public void actualizar(Test test) {
        testRef.child(test.getId()).setValue(test);
    }
}



