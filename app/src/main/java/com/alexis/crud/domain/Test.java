package com.alexis.crud.domain;

public class Test {

    private String id;
    private String valor1;
    private String valor2;

    public String getValor1() {
        return valor1;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setValor1(String valor1) {
        this.valor1 = valor1;
    }

    public String getValor2() {
        return valor2;
    }

    public void setValor2(String valor2) {
        this.valor2 = valor2;
    }
}
