package com.alexis.crud.adapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.alexis.crud.MainActivity;
import com.alexis.crud.R;
import com.alexis.crud.domain.Test;

import java.util.ArrayList;
import java.util.List;

public class ListaAdapter extends RecyclerView.Adapter<ListaAdapter.ViewHolder> {

    private List<Test> list = new ArrayList<>();
    private Context context;
    private MainActivity mainActivity;


    public ListaAdapter(Context context, MainActivity activity,List<Test> list) {
        this.context = context;
        this.mainActivity = activity;
        this.list = list;
    }

    public void addTest(Test test) {
        list.add(test);
        notifyItemInserted(list.size());
    }

    public void removeTest(Test test){
        list.remove(test);
        notifyItemInserted(list.size());
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.detalle_card, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder view, int i) {
        view.tv1.setText(list.get(i).getValor1());
        view.tv2.setText(list.get(i).getValor2());
        final int posicion = i;
        view.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Dialog dialog = new Dialog(mainActivity);
                dialog.setContentView(R.layout.edit_layout);
                final EditText et1, et2;
                Button editar, eliminar;
                et1 = (EditText) dialog.findViewById(R.id.edit_1);
                et2 = (EditText) dialog.findViewById(R.id.edit_2);
                et1.setText(list.get(posicion).getValor1());
                et2.setText(list.get(posicion).getValor2());
                editar = (Button) dialog.findViewById(R.id.edEditar);
                eliminar = (Button) dialog.findViewById(R.id.edEliminar);
                editar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Test test = list.get(posicion);
                        test.setValor1(et1.getText().toString());
                        test.setValor2(et2.getText().toString());
                        mainActivity.actualizar(test);
                        Toast.makeText(context,"Actualizado",Toast.LENGTH_LONG).show();
                        dialog.dismiss();
                    }
                });

                eliminar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        mainActivity.eliminar(list.get(posicion));
                        Toast.makeText(context,"Eliminado",Toast.LENGTH_LONG).show();
                        dialog.dismiss();
                    }
                });
                dialog.show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tv1, tv2;
        private CardView cardView;

        public ViewHolder(View view) {
            super(view);
            tv1 = (TextView) view.findViewById(R.id.tv1);
            tv2 = (TextView) view.findViewById(R.id.tv2);
            cardView = (CardView) view.findViewById(R.id.cv);
        }
    }


}
